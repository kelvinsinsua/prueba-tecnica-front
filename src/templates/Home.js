import React, {useState} from 'react';

import Layout from '../components/Layout'
import Title from '../components/Title'
import Input from '../components/Input'
import Button from '../components/Button'
import Error from '../components/Error'
import Success from '../components/Success'
import API from '../api'


const Home = () => {

    const [document, setDocument] = useState('');
    const [fullName, setFullName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');

    const [documentError, setDocumentError] = useState('');
    const [fullNameError, setFullNameError] = useState('');
    const [emailError, setEmailError] = useState('');
    const [phoneError, setPhoneError] = useState('');

    const [loading, setLoading] = useState();
    const [successMsg, setSuccessMsg] = useState();

    const register = () => {
        setLoading(true);
        clear()
        const data = new FormData();
        data.append('document',document);
        data.append('fullName',fullName);
        data.append('email',email);
        data.append('phone',phone);

        API.post(`/api/register`,data).then(({data}) => {
            setLoading(false)
            if(data.code === 200){
                setSuccessMsg(data.message)
            }else{
                data.errors.forEach(element => {
                    if(element.email){
                        setEmailError(element.email)
                    }
                    if(element.document){
                        setDocumentError(element.document)
                    }
                    if(element.fullName){
                        setFullNameError(element.fullName)
                    }
                    if(element.phone){
                        setPhoneError(element.phone)
                    }
                });
            }
          });
    }

    const clear = () => {
        setEmailError('')
        setDocumentError('')
        setFullNameError('')
        setPhoneError('')
        setSuccessMsg('')
    }
    return(
        <Layout>
            <Title>Regístrate</Title>
            <Input
                placeholder="Documento"
                onChange={({target}) => setDocument(target.value)} 
            />
            <Error>{documentError}</Error>
            <Input
                placeholder="Nombres"
                onChange={({target}) => setFullName(target.value)} 
            />
            <Error>{fullNameError}</Error>
            <Input
                placeholder="Email" 
                onChange={({target}) => setEmail(target.value)} 
            />
            <Error>{emailError}</Error>
            <Input
                placeholder="Celular" 
                onChange={({target}) => setPhone(target.value)} 
            />
            <Error>{phoneError}</Error>

            <Success>{successMsg}</Success>

            <div className="total-center">
                {loading ? <p>Cargando...</p> : <Button onClick={register}>Registrarme</Button>}
            </div>
        </Layout>
    )
}

export default Home;