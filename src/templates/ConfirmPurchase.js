import React, {useState} from 'react';

import Layout from '../components/Layout'
import Title from '../components/Title'
import Input from '../components/Input'
import Button from '../components/Button'
import Error from '../components/Error'
import Success from '../components/Success'
import API from '../api'


const ConfirmPurchase = (props) => {

    const [code, setCode] = useState('');

    const [codeError, setCodeError] = useState('');

    const [loading, setLoading] = useState();
    const [successMsg, setSuccessMsg] = useState();

    const add = () => {
        setLoading(true);
        clear()
        const data = new FormData();
        data.append('token',code);
        data.append('sessionId',props.match.params.sessionId);

        API.post(`/api/payment`,data).then(({data}) => {
            setLoading(false)
            if(data.code === 200){
                setSuccessMsg(data.message)
            }else{
                data.errors.forEach(element => {
                    if(element.token){
                        setCodeError(element.token)
                    }
                    if(element.purchase){
                        setCodeError(element.purchase)
                    }
                });
            }
          });
    }

    const clear = () => {
        setCodeError('')
        setSuccessMsg('')
    }
    return(
        <Layout>
            <Title>Confirmar compra</Title>
            <Input
                placeholder="Código"
                onChange={({target}) => setCode(target.value)} 
            />
            <Error>{codeError}</Error>      

            <Success>{successMsg}</Success>

            <div className="total-center">
                {loading ? <p>Cargando...</p> : <Button onClick={add}>Confirmar</Button>}
            </div>
        </Layout>
    )
}

export default ConfirmPurchase;