import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Home';
import AddBalance from './AddBalance';
import Purchase from './Purchase';
import Balance from './Balance';
import ConfirmPurchase from './ConfirmPurchase';

const Routes = () => {
  return (
    <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/add-balance" component={AddBalance} />
        <Route exact path="/purchase" component={Purchase} />
        <Route exact path="/balance" component={Balance} />
        <Route exact path="/confirm/:sessionId" component={ConfirmPurchase} />
    </Router>
  );
};


export default Routes;
