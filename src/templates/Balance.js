import React, {useState} from 'react';

import Layout from '../components/Layout'
import Title from '../components/Title'
import Input from '../components/Input'
import Button from '../components/Button'
import Error from '../components/Error'
import Success from '../components/Success'
import API from '../api'


const Balance = () => {

    const [document, setDocument] = useState('');
    const [phone, setPhone] = useState('');

    const [documentError, setDocumentError] = useState('');
    const [phoneError, setPhoneError] = useState('');

    const [loading, setLoading] = useState();
    const [successMsg, setSuccessMsg] = useState();

    const add = () => {
        setLoading(true);
        clear()

        API.get(`/api/balance?document=${document}&phone=${phone}`).then(({data}) => {
            setLoading(false)
            if(data.code === 200){
                setSuccessMsg('Su saldo actual es de: '+data.payload.balance)
            }else{
                data.errors.forEach(element => {
                    if(element.document){
                        setDocumentError(element.document)
                    }
                    if(element.client){
                        setDocumentError(element.client)
                    }
                    if(element.phone){
                        setPhoneError(element.phone)
                    }
                });
            }
          });
    }

    const clear = () => {
        setDocumentError('')
        setPhoneError('')
        setSuccessMsg('')
    }
    return(
        <Layout>
            <Title>Consultar saldo</Title>
            <Input
                placeholder="Documento"
                onChange={({target}) => setDocument(target.value)} 
            />
            <Error>{documentError}</Error>            
            <Input
                placeholder="Celular" 
                onChange={({target}) => setPhone(target.value)} 
            />
            <Error>{phoneError}</Error>

            <Success>{successMsg}</Success>

            <div className="total-center">
                {loading ? <p>Cargando...</p> : <Button onClick={add}>Consultar</Button>}
            </div>
        </Layout>
    )
}

export default Balance;