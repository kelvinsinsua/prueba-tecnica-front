import React, {useState} from 'react';

import Layout from '../components/Layout'
import Title from '../components/Title'
import Input from '../components/Input'
import Button from '../components/Button'
import Error from '../components/Error'
import Success from '../components/Success'
import API from '../api'


const Purchase = () => {

    const [document, setDocument] = useState('');
    const [amount, setAmount] = useState('');
    const [phone, setPhone] = useState('');

    const [documentError, setDocumentError] = useState('');
    const [amountError, setAmountError] = useState('');
    const [phoneError, setPhoneError] = useState('');

    const [loading, setLoading] = useState();
    const [successMsg, setSuccessMsg] = useState();

    const add = () => {
        setLoading(true);
        clear()
        const data = new FormData();
        data.append('document',document);
        data.append('amount',amount);
        data.append('phone',phone);

        API.post(`/api/purchase`,data).then(({data}) => {
            setLoading(false)
            if(data.code === 200){
                setSuccessMsg(data.message)
            }else{
                data.errors.forEach(element => {
                    if(element.amount){
                        setAmountError(element.amount)
                    }
                    if(element.document){
                        setDocumentError(element.document)
                    }
                    if(element.client){
                        setDocumentError(element.client)
                    }
                    if(element.phone){
                        setPhoneError(element.phone)
                    }
                });
            }
          });
    }

    const clear = () => {
        setAmountError('')
        setDocumentError('')
        setPhoneError('')
        setSuccessMsg('')
    }
    return(
        <Layout>
            <Title>Realizar compra</Title>
            <Input
                placeholder="Documento"
                onChange={({target}) => setDocument(target.value)} 
            />
            <Error>{documentError}</Error>            
            <Input
                placeholder="Celular" 
                onChange={({target}) => setPhone(target.value)} 
            />
            <Error>{phoneError}</Error>
            <Input
                placeholder="Monto" 
                onChange={({target}) => setAmount(target.value)} 
            />
            <Error>{amountError}</Error>

            <Success>{successMsg}</Success>

            <div className="total-center">
                {loading ? <p>Cargando...</p> : <Button onClick={add}>Comprar</Button>}
            </div>
        </Layout>
    )
}

export default Purchase;