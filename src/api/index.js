import axios from 'axios';

const BASE_URL = 'http://pruebatecnica-api.insigniadev.com';

const API = axios.create({
  baseURL: BASE_URL,
  timeout: 30000000,
});

export default API;