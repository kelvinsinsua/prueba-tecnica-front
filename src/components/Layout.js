import React from 'react';
import styled from 'styled-components'


const Layout = (props) => {
    return(
        <div>
            <Header >
                <MenuItem href="/">Registro</MenuItem>
                <MenuItem href="/add-balance">Recarga</MenuItem>
                <MenuItem href="/purchase">Realizar compra</MenuItem>
                <MenuItem href="/balance">Consultar</MenuItem>
            </Header>
            <Banner />
            <CardContainer>
                <Card>
                    {props.children}
                </Card>
            
            </CardContainer>
    </div>
    )
}

const Header = styled.div`
    height: 50px;
    background-color: #FFF;
    width: 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
    /* padding-right: 15px; */
`

const MenuItem = styled.a`
    font-size: 14px;
    font-weight: bold;
    color: #707070;
    text-align: center;
    margin-right: 10px;
    text-decoration: none;
`

const Banner = styled.div`
    width: 100%;
    height: 270px;
    background-image: url("/images/access-fondo.jpg");
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
`

const CardContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-top: -180px;
    margin-bottom: 30px;
    margin-left: 15px;
    margin-right: 15px;
    
    @media (max-width: 500px){
        margin-top: -200px;
    }
`
const Card = styled.div`
    border-radius: 10px;
    box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.1);
    background-color: #ffffff;
    min-width: 420px;
    max-width: 450px;
    min-height: 100px;
    padding: 30px;
    @media (max-width: 500px){
        max-width: 340px;
        min-width: 240px;
    }
`

export default Layout;