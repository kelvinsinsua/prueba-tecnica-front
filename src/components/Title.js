import React from 'react';
import styled from 'styled-components'

const Title = (props) => {
    return(
        <Text>{props.children}</Text>
    )
}

const Text = styled.p`
    font-size: 18pt;
    font-weight: bold;
    color: #707070;
    text-align: center;
`

export default Title;