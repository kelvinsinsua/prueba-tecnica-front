import React from 'react';
import styled from 'styled-components'

const Success = (props) => {
    return(
        <Text>{props.children}</Text>
    )
}

const Text = styled.div`
    font-size: 12px;
    font-weight: bold;
    color: green;
    text-align: center;
`

export default Success;