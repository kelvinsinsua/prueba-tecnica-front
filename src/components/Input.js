import styled from "styled-components";

const Input = styled.input`
  width: ${props => props.width || "100%"};
  height: ${props => props.height || "35px"};
  padding-bottom: ${props => props.paddingBottom};
 padding-left: ${props => props.paddingLeft || "5px"};
  border-radius: ${props => props.borderRadius || "5px"};
  border: ${props => props.border || "solid 1px #b7b8b8"};
  outline-color: #000;
  background-color: #fff;
  outline: none;
  cursor: ${props => props.cursor};
  margin-top: 10px;
  text-align: ${props => props.textAlign || "left"};
  @media (max-width: ${props => props.sizeSm || "480px"}) {
    width: ${props => props.widthI || "280px"};
  }
`;

export default Input;