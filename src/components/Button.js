import styled from "styled-components";

const Button = styled.div`
  background-color: ${props => props.backgroundColor || "#F24A44"};
  color: ${props => props.color || "#FFF"};
  font-family: ${props => props.fontFamily || "Open Sans"}, sans-serif;
  font-weight: ${props => props.fontWeight || "400"};
  width: ${props => props.width || "50%"};
  max-width: ${props => props.maxWidth};
  height: ${props => props.height || "40px"};
  border: ${props => props.border};
  margin-top: ${props => props.marginTop || '10px'};
  margin-bottom: ${props => props.marginBottom};
  margin-left: ${props => props.marginLeft};
  border-radius: ${props => props.borderRadius || "4px"};
  border-bottom-left-radius: ${props => props.borderBottom};
  border-top-left-radius: ${props => props.borderTop};
  padding: 0px 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  :hover {
    opacity: 0.8;
  }
  :active {
    opacity: 0.1;
  }
  @media (max-width: ${props => props.sizeSm || "480px"}) {
    width: ${props => props.widthB || "280px"};
  }
`;

export default Button;



