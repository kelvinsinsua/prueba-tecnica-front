import React from 'react';
import styled from 'styled-components'

const Error = (props) => {
    return(
        <Text>{props.children}</Text>
    )
}

const Text = styled.div`
    font-size: 12px;
    font-weight: bold;
    color: red;
    text-align: center;
`

export default Error;